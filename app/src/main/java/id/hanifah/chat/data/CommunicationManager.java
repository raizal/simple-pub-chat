package id.hanifah.chat.data;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by raizal.pregnanta on 15/10/18.
 */

public class CommunicationManager {

    private String server;
    private User user;
    private OnMessageListener listener;
    private Socket socket;

    public CommunicationManager() {
    }

    public static CommunicationManager create(String server, User user, OnMessageListener listener) {
        CommunicationManager communicationManager = new CommunicationManager();

        //set user reference
        communicationManager.user = user;

        communicationManager.setServer(server);
        communicationManager.setListener(listener);
        return communicationManager;
    }

    private void setListener(OnMessageListener listener) {
        this.listener = listener;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public void sendUserData() {
        android.util.Log.w("SEND USER DATA", "TRYING SEND USER DATA " + user.getName());
        if (socket != null && socket.connected()) {
            android.util.Log.w("SEND USER DATA", "SENT USER DATA " + user.getName());
            socket.emit("user", user.getName());
        }
    }

    public void sendMessage(String message) {
        if (socket != null && socket.connected()) {
            socket.emit("chat", message);
        }
    }

    public void connect() {
        try {
            if (socket != null) socket.disconnect();
        }catch (Exception e) {
        }
        try {
            //set server url
            IO.Options options = new IO.Options();
            options.forceNew = true;
            options.reconnectionDelay = 500;

            socket = IO.socket(server);
            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    android.util.Log.e("ON CONNECTED", "CONNECTED WITH " + server);
                    sendUserData();
                }
            });

            //set listener for incoming message
            socket.on("log", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    //ignore incoming message if listener is not set
                    if (listener != null) {
                        //checking incoming message data
                        for (Object arg : args) {
                            //ignore if null
                            if (arg != null) {
                                //convert message into Log
                                Log log = Log.fromString(arg.toString());
                                //send to listener if Log not null
                                if (log != null) {
                                    listener.onMessage(log);
                                }
                            }
                        }
                    }
                }
            });
            //connect to socket.io server
            socket.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public interface OnMessageListener {
        public void onMessage(Log log);
    }
}
