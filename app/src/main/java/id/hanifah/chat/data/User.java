package id.hanifah.chat.data;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by raizal.pregnanta on 15/10/18.
 */

public class User {
    public static final String DEFAULT_NAME = "Anonymous";
    private Context context;
    private SharedPreferences sharedPreferences;
    private String name;

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        if (sharedPreferences != null) {
            sharedPreferences.edit().putString("name", name).commit();
        }
    }

    public static User init(Context context){
        User user = new User();
        user.sharedPreferences = context.getSharedPreferences("config", Context.MODE_PRIVATE);
        user.name = user.sharedPreferences.getString("name", DEFAULT_NAME);
        return user;
    }
}
