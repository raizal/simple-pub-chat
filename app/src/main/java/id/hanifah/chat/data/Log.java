package id.hanifah.chat.data;

import com.google.gson.Gson;

/**
 * Created by raizal.pregnanta on 15/10/18.
 */

public class Log {
    private String from;
    private String message;
    private long timestamp;

    public long getTimestamp() {
        return timestamp;
    }

    public Log() {
    }

    public String getFrom() {

        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static Log fromString(String data){
        try {
            Log log = new Gson().fromJson(data, Log.class);
            return log;
        } catch (Exception e){
            return null;
        }
    }
}
