package id.hanifah.chat;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import net.idik.lib.slimadapter.SlimAdapter;
import net.idik.lib.slimadapter.SlimInjector;
import net.idik.lib.slimadapter.viewinjector.IViewInjector;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.hanifah.chat.data.CommunicationManager;
import id.hanifah.chat.data.Log;
import id.hanifah.chat.data.User;

public class MainActivity extends AppCompatActivity {
    private static final String DEFAULT_SERVER = "http://178.128.119.68:3000/";

    private User currentUser;
    private List<Log> data = new ArrayList<>();
    private RecyclerView recyclerView;
    private EditText inputMessage;
    private SlimAdapter adapter;
    private CommunicationManager communicationManager;
    private SharedPreferences setting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setting = getSharedPreferences("setting", MODE_PRIVATE);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        inputMessage = findViewById(R.id.chat_text);
        findViewById(R.id.button_send).setOnClickListener(onSendClick);

        recyclerView = findViewById(R.id.recyclerview);
        initRecyclerView();

        currentUser = User.init(this);
        refreshName();

        communicationManager = CommunicationManager.create(setting.getString("server", DEFAULT_SERVER), currentUser, messageListener);
        communicationManager.connect();

        if (currentUser != null && currentUser.getName().equals(User.DEFAULT_NAME)) {
            showAskNameDialog();
        }
    }

    private void refreshName() {
        if (currentUser != null) {
            getSupportActionBar().setTitle(currentUser.getName());
        }
    }

    private void initRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = SlimAdapter.create()
                .register(R.layout.chat_item, new SlimInjector<Log>() {
                    @Override
                    public void onInject(Log data, IViewInjector injector) {
                        injector.text(R.id.text_from, data.getFrom());
                        injector.text(R.id.text_message, data.getMessage());
                        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                        Date date = new Date(data.getTimestamp());
                        injector.text(R.id.timestamp_text, formatter.format(date));
                    }
                })
                .updateData(data)
                .attachTo(recyclerView);
    }

    private CommunicationManager.OnMessageListener messageListener = new CommunicationManager.OnMessageListener() {
        @Override
        public void onMessage(final Log log) {
            data.add(log);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (adapter != null) {
                        adapter.notifyItemInserted(data.size() - 1);
                        recyclerView.scrollToPosition(data.size() - 1);
                        if (!log.getFrom().equals(currentUser.getName()) && !log.getFrom().toLowerCase().equals("system")) {
                            try {
                                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                                r.play();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
        }
    };

    private View.OnClickListener onSendClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            communicationManager.sendMessage(inputMessage.getText().toString());
            inputMessage.setText("");
        }
    };

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_user:
                showAskNameDialog();
                break;
            case R.id.setup:
                showChangeServerDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void showAskNameDialog() {
        View form = getLayoutInflater().inflate(R.layout.ask_name_dialog, null);
        final EditText inputName = form.findViewById(R.id.user_name_text);
        if (currentUser != null && !currentUser.getName().equals(User.DEFAULT_NAME)) {
            inputName.setText(currentUser.getName());
        }
        new AlertDialog.Builder(this)
                .setView(form)
                .setTitle("Welcome")
                .setCancelable(false)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (currentUser != null && inputName != null) {
                            Toast.makeText(MainActivity.this, inputName.getText().toString(), Toast.LENGTH_SHORT).show();
                            currentUser.setName(inputName.getText().toString());
                            refreshName();
                            communicationManager.sendUserData();
                        }
                    }
                })
                .setNegativeButton("Close App", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .show();
    }

    protected void showChangeServerDialog() {
        View form = getLayoutInflater().inflate(R.layout.ask_name_dialog, null);
        final EditText inputName = form.findViewById(R.id.user_name_text);
        inputName.setHint("Your server address");
        if (communicationManager != null) {
            inputName.setText(communicationManager.getServer());
        }
        new AlertDialog.Builder(this)
                .setView(form)
                .setTitle("Change your server address")
                .setCancelable(false)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (currentUser != null && inputName != null && inputName.getText().length() > 0) {
                            // don't reconnect if server url is same with current setting
                            if (communicationManager.getServer().equals(inputName.getText().toString())) return;
                            String server = inputName.getText().toString();
                            communicationManager.setServer(server);
                            setting.edit().putString("server", server).commit();
                            communicationManager.connect();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }
}
