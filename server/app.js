var app = require('express')();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
console.log('SERVER START')
app.get('/', function (req, res){
	res.send('Server Ready')
})
const DEFAULT_NAME = 'Anonymous'

const createMessage = (from, message) => ({
  from,
  message,
  timestamp: Date.now()
})

io.on('connection', (socket) => {
  var user = {
    name: DEFAULT_NAME
  }
  console.log('New user is connected')
  socket.on('user', (data) => {
    const message = user.name === DEFAULT_NAME || user.name === data ? `${data} is connected` : `${user.name} changed his name into ${data}`
    user.name = data
    console.log('send back user => ', data)
    io.emit('log', createMessage('System', message))
  })

  socket.on('chat', (data) => {
    console.log('user => ', user, 'chat => ', data)
    io.emit('log', createMessage(user.name, data))
  })

  socket.on('disconnected', () => {
    console.log('user => ', user, 'disconnected => ', data)
    io.emit('log', createMessage('System', `${user.name} is disconnected`))
  })

});
server.listen(3000);